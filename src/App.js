import React from "react";
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
import { StateMachineProvider, createStore } from "little-state-machine";

import logo from "../src/assets/headerLogo.svg";

import "./App.scss";
import Page1 from "./pages/Page1";
import Page2 from "./pages/Page2";
import Page3 from "./pages/Page3";
import Page0 from "./pages/Page0";

createStore({
  data: {},
});

function App() {
  return (
    <BrowserRouter>
      <header className="App-header">
        <Link to={"/"}>
          <img src={logo} className="App-logo" alt="logo" />
        </Link>
      </header>

      <StateMachineProvider>
        <Switch>
          <Route exact path="/">
            <Page0 />
          </Route>
          <Route exact path="/step-1">
            <Page1 />
          </Route>
          <Route exact path="/step-2">
            <Page2 />
          </Route>
          <Route exact path="/step-3">
            <Page3 />
          </Route>
        </Switch>
      </StateMachineProvider>
    </BrowserRouter>
  );
}

export default App;
