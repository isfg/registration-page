import React from "react";
import "./_page2.scss";
import { Radio } from "@material-ui/core";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Controller, useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { useStateMachine } from "little-state-machine";
import updateAction from "./updateAction";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  radioGroup: {
    display: "flex",
    flexDirection: "row",
    marginBottom: "2rem",
  },
  radio: {
    marginRight: "2rem",
  },
});

const Page2 = () => {
  const history = useHistory();
  const { handleSubmit, register, control } = useForm();
  const { action } = useStateMachine(updateAction);
  const styles = useStyles();

  const onSubmitStepTwo = (values) => {
    console.log(values);
    history.push("/step-3");
    action(values);
  };

  return (
    <div className="App2">
      <main>
        <form onSubmit={handleSubmit(onSubmitStepTwo)}>
          <h2>Podrobnosti vedení spisové služby:</h2>
          <section>
            <div>
              <h5>Přibližný počet uživatelů: </h5>
              <div className={"pocet-radio-container"}>
                <div>
                  <Controller
                    as={
                      <RadioGroup name={"amount"} className={styles.radioGroup}>
                        <FormControlLabel
                          className={styles.radio}
                          control={<Radio color="primary" />}
                          disabled={false}
                          label={"0 - 5"}
                          value="0 - 5"
                        />
                        <FormControlLabel
                          className={styles.radio}
                          control={<Radio color="primary" />}
                          disabled={false}
                          label={"5 - 10"}
                          value="5 - 10"
                        />
                        <FormControlLabel
                          className={styles.radio}
                          control={<Radio color="primary" />}
                          disabled={false}
                          label={"10 a více"}
                          value={"10 a více"}
                        />
                      </RadioGroup>
                    }
                    name="amount"
                    control={control}
                  />
                </div>
              </div>
              <div>
                <h5>Spisový řád: </h5>
                <Controller
                  as={
                    <RadioGroup
                      name={"DataRules"}
                      className={styles.radioGroup}
                    >
                      <FormControlLabel
                        className={styles.radio}
                        control={<Radio color="primary" />}
                        label={"Mám Spisový řád připraven na ESSL"}
                        value={"Mám Spisový řád připraven na ESSL"}
                      />
                      <FormControlLabel
                        className={styles.radio}
                        control={<Radio color="primary" />}
                        label={"Chci pomoct s vytvořením Spisového řádu"}
                        value={"Chci pomoct s vytvořením Spisového řádu"}
                      />
                    </RadioGroup>
                  }
                  name="DataRules"
                  control={control}
                />
              </div>
            </div>
            <div>
              <h5>Spisový plán: </h5>
              <Controller
                as={
                  <RadioGroup name={"DataPlan"} className={styles.radioGroup}>
                    <FormControlLabel
                      className={styles.radio}
                      control={<Radio color="primary" />}
                      label={"Mám Spisový plán ve formátu XML"}
                      value={"Mám Spisový plán ve formátu XML"}
                    />
                    <FormControlLabel
                      className={styles.radio}
                      control={<Radio color="primary" />}
                      label={"Mám Spisový plán v jiném formátu"}
                      value={"Mám Spisový plán v jiném formátu"}
                    />
                    <FormControlLabel
                      className={styles.radio}
                      control={<Radio color="primary" />}
                      label={"Nemám Spisový plán a chci s ním pomoct"}
                      value={"Nemám Spisový plán a chci s ním pomoct"}
                    />
                  </RadioGroup>
                }
                name="DataPlan"
                control={control}
              />
            </div>
            <div>
              <h5>Analýza: </h5>
              <Controller
                as={
                  <RadioGroup name={"Analysis"} className={styles.radioGroup}>
                    <FormControlLabel
                      className={styles.radio}
                      control={<Radio color="primary" />}
                      label={
                        "Mám analytickou a projektovou dokumentaci v souladu s legislativou"
                      }
                      value={
                        "Mám analytickou a projektovou dokumentaci v souladu s legislativou"
                      }
                    />
                    <FormControlLabel
                      className={styles.radio}
                      control={<Radio color="primary" />}
                      label={"Mám zájem o analýzu Spisové služby"}
                      value={"Mám zájem o analýzu Spisové služby"}
                    />
                  </RadioGroup>
                }
                name="Analysis"
                control={control}
              />
            </div>
            <article>
              <h5>Administrátor ESSL: </h5>

              <section className={"administrator-form-wrapper"}>
                <div className={"input-and-label-wrapper"}>
                  <h5>Jméno a příjmení* </h5>

                  <input
                    ref={register}
                    type={"text"}
                    name="competentAdministratorESSLNameAndSurnamePerson"
                  />
                </div>
                <div className={"input-and-label-wrapper"}>
                  <h5>E-mail* </h5>

                  <input
                    ref={register}
                    type={"text"}
                    name="competentAdministratorESSLEmailPerson"
                  />
                </div>
                <div className={"input-and-label-wrapper"}>
                  <h5>Telefon </h5>

                  <input
                    ref={register}
                    type={"text"}
                    name="competentAdministratorESSLPhonePerson"
                  />
                </div>
              </section>
            </article>
          </section>

          <div className={"button-wrapper"}>
            <button>
              <h3>Dokončit</h3>
            </button>
          </div>
        </form>
      </main>
    </div>
  );
};

export default Page2;
