import React from "react";
import { useStateMachine } from "little-state-machine";
import updateAction from "./updateAction";

import "./_page3.scss";

const Page3 = () => {
  const { state } = useStateMachine(updateAction);
  return (
    <div>
      <div className="App3">
        <main>
          <h1>Stav přípravy ESSL</h1>

          <form>
            <h2>Údaje o organizaci:</h2>

            <section>
              <div>
                <h5>IČ </h5>

                <input
                  readOnly
                  type={"text"}
                  name="ico"
                  value={state.data.ico}
                />
              </div>
              <div>
                <h5>Název organizace </h5>

                <input
                  type={"text"}
                  readOnly
                  name="organizationName"
                  value={state.data.organizationName}
                />
              </div>
              <div>
                <h5>ID datové schránky </h5>

                <input
                  readOnly
                  type={"text"}
                  name="dataBoxId"
                  value={state.data.dataBoxId}
                />
              </div>
              <div>
                <h5>Jméno vedoucího organizace </h5>

                <input
                  type={"text"}
                  readOnly
                  name="organizationLeaderName"
                  value={state.data.organizationLeaderName}
                />
              </div>
              <div>
                <h5>Příjmení vedoucího organizace </h5>

                <input
                  type={"text"}
                  readOnly
                  name="organizationLeaderSurname"
                  value={state.data.organizationLeaderSurname}
                />
              </div>
            </section>

            <h2>Údaje o osobách zodpovědných za spisovou schránku:</h2>

            <section>
              <div>
                <h5>Jméno a příjmení</h5>

                <input
                  type={"text"}
                  readOnly
                  name="competentDataBoxNameAndSurnamePerson"
                  value={state.data.competentDataBoxNameAndSurnamePerson}
                />
              </div>
              <div>
                <h5>E-mail </h5>

                <input
                  type={"text"}
                  readOnly
                  name="competentDataBoxEmailPerson"
                  value={state.data.competentDataBoxEmailPerson}
                />
              </div>
              <div>
                <h5>Telefon </h5>

                <input
                  type={"text"}
                  readOnly
                  name="competentDataBoxPhonePerson"
                  value={state.data.competentDataBoxPhonePerson}
                />
              </div>
            </section>
            {state.data.competentDataBoxNameAndSurnamePerson2 && (
              <section>
                <div>
                  <h5>Jméno a příjmení</h5>

                  <input
                    type={"text"}
                    readOnly
                    name="competentDataBoxNameAndSurnamePerson2"
                    value={state.data.competentDataBoxNameAndSurnamePerson2}
                  />
                </div>
                <div>
                  <h5>E-mail </h5>

                  <input
                    type={"text"}
                    readOnly
                    name="competentDataBoxEmailPerson2"
                    value={state.data.competentDataBoxEmailPerson2}
                  />
                </div>
                <div>
                  <h5>Telefon </h5>

                  <input
                    type={"text"}
                    readOnly
                    name="competentDataBoxPhonePerson2"
                    value={state.data.competentDataBoxPhonePerson2}
                  />
                </div>
              </section>
            )}
            <h2>Údaje o osobě zodpovědné za technickou správu:</h2>
            <section>
              <div>
                <h5>Jméno a příjmení </h5>

                <input
                  type={"text"}
                  readOnly
                  name="competentTechnicalManagementNameAndSurnamePerson"
                  value={
                    state.data.competentTechnicalManagementNameAndSurnamePerson
                  }
                />
              </div>
              <div>
                <h5>E-mail </h5>

                <input
                  readOnly
                  type={"text"}
                  name="competentTechnicalManagementEmailPerson"
                  value={state.data.competentTechnicalManagementEmailPerson}
                />
              </div>
              <div>
                <h5>Telefon </h5>

                <input
                  readOnly
                  type={"text"}
                  name="competentTechnicalManagementPhonePerson"
                  value={state.data.competentTechnicalManagementPhonePerson}
                />
              </div>
            </section>
          </form>
          <article>
            <section>
              <h2>Stav schválení MHMP:</h2>
              <h4>Schváleno</h4>
            </section>
            <section>
              <h2>Stav schválení OICT:</h2>
              <h4>Schváleno</h4>
            </section>
            <section>
              <h2>Stav schválení analýzy:</h2>
              <h4>Schváleno</h4>
            </section>
            <section>
              <h2>Stav přípravy ESSL:</h2>
              <h4>Připraveno</h4>
            </section>
          </article>
          <article className={"text-area-wrapper"}>
            <h2>Máte otázky k přípravě ESSL?</h2>

            <textarea />
            <div className={"button-wrapper"}>
              <button>
                <h3>Odeslat dotaz</h3>
              </button>
            </div>
          </article>
        </main>
      </div>
    </div>
  );
};

export default Page3;
