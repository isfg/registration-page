import React from "react";
import logo from "../assets/loginlogo.png";
import "./_pages0.scss";
import { Link, useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { useStateMachine } from "little-state-machine";
import updateAction from "./updateAction";

const Page0 = () => {
  const history = useHistory();
  const { register, handleSubmit } = useForm();
  const { action } = useStateMachine(updateAction);

  const onSubmitStepOne = (values) => {
    action(values);
    history.push("/step-1");
  };

  return (
    <div className="App0">
      <main>
        <form onSubmit={handleSubmit(onSubmitStepOne)}>
          <img src={logo} className="App-logo" alt="logo" />

          <section>
            <div>
              <h5>Jméno a příjmení </h5>

              <input ref={register} type={"text"} name="userName" />
            </div>
            <div>
              <h5>E-mail</h5>

              <input ref={register} type={"text"} name="email" />
            </div>
          </section>
          <div className={"button-wrapper"}>
            <button>
              <h3>Registrovat organizaci</h3>
            </button>
          </div>
        </form>
      </main>
    </div>
  );
};

export default Page0;
