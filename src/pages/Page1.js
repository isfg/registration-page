import React, { useState } from "react";

import "./_page1.scss";
import { useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { useStateMachine } from "little-state-machine";
import updateAction from "./updateAction";
import { Button } from "@material-ui/core";

const Page1 = () => {
  const history = useHistory();
  const { register, handleSubmit } = useForm();
  const { action, state } = useStateMachine(updateAction);
  const [clicked, setClicked] = useState(false);
  const onSubmitStepOne = (values) => {
    action(values);
    history.push("/step-2");
  };

  return (
    <div className="App">
      <main>
        <form onSubmit={handleSubmit(onSubmitStepOne)}>
          <h2>Zaregistrujte svoji organizaci:</h2>
          <section>
            <div>
              <h5>IČ* </h5>

              <input ref={register} type={"text"} name="ico" />
            </div>
            <div>
              <h5>Název organizace* </h5>

              <input ref={register} type={"text"} name="organizationName" />
            </div>
            <div>
              <h5>ID datové schránky* </h5>

              <input type={"text"} ref={register} name="dataBoxId" />
            </div>
            <div>
              <h5>Jméno vedoucího organizace* </h5>

              <input
                type={"text"}
                ref={register}
                name="organizationLeaderName"
              />
            </div>
            <div>
              <h5>Příjmení vedoucího organizace* </h5>

              <input
                ref={register}
                type={"text"}
                name="organizationLeaderSurname"
              />
            </div>
          </section>

          <h2>Údaje o osobách zodpovědných za spisovou schránku:</h2>

          <article>
            <section>
              <div>
                <h5>Jméno a příjmení* </h5>

                <input
                  value={state.data.userName}
                  ref={register}
                  type={"text"}
                  name="competentDataBoxNameAndSurnamePerson"
                />
              </div>
              <div>
                <h5>E-mail* </h5>

                <input
                  value={state.data.email}
                  ref={register}
                  type={"text"}
                  name="competentDataBoxEmailPerson"
                />
              </div>
              <div>
                <h5>Telefon </h5>

                <input
                  type={"text"}
                  ref={register}
                  name="competentDataBoxPhonePerson"
                />
              </div>
            </section>
          </article>

          {clicked && (
            <article>
              <section>
                <div>
                  <h5>Jméno a příjmení* </h5>

                  <input
                    ref={register}
                    type={"text"}
                    name="competentDataBoxNameAndSurnamePerson2"
                  />
                </div>
                <div>
                  <h5>E-mail* </h5>

                  <input
                    ref={register}
                    type={"text"}
                    name="competentDataBoxEmailPerson2"
                  />
                </div>
                <div>
                  <h5>Telefon </h5>

                  <input
                    type={"text"}
                    ref={register}
                    name="competentDataBoxPhonePerson2"
                  />
                </div>
              </section>
            </article>
          )}
          <div className={"plus-button-wrapper"}>
            <button
              onClick={(event) => {
                event.preventDefault();
                setClicked(true);
              }}
            >
              Přidat další osobu
            </button>
          </div>
          <h2>Údaje o osobě zodpovědné za technickou správu:</h2>
          <article>
            <section>
              <div>
                <h5>Jméno a příjmení* </h5>

                <input
                  ref={register}
                  type={"text"}
                  name="competentTechnicalManagementNameAndSurnamePerson"
                />
              </div>
              <div>
                <h5>E-mail* </h5>

                <input
                  ref={register}
                  type={"text"}
                  name="competentTechnicalManagementEmailPerson"
                />
              </div>
              <div>
                <h5>Telefon </h5>

                <input
                  ref={register}
                  type={"text"}
                  name="competentTechnicalManagementPhonePerson"
                />
              </div>
            </section>
          </article>
          <div className={"button-wrapper"}>
            <button>
              <h3>Další</h3>
            </button>
          </div>
        </form>
      </main>
    </div>
  );
};

export default Page1;
